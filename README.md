# Basic Grunt App

Esta es una plantilla básica de Grunt.

## Instrucciones

### Instalación

1. Clonar el repositorio
2. Instalar las dependencias (`npm install`)

### Ejecución

Ejecutar el comando `npm start`.
